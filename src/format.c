#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>

#include "format.h"

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))

size_t length_in_symbols(const char *str) {
	size_t result = 0;
	for (const char *i = str; *i != '\0'; ++i) {
		i += *i != ' ' && *i != '(' && *i != ')';
		++result;
	}
	return result;
}

uint8_t print_grade_table(const GradeTable table) {
	uint8_t longest_subject_length = 0;
	uint8_t longest_grade_sequence = 0;

	for (uint8_t i = 0; i < table.length; ++i) {
		const GradeTableEntry *entry = table.entries + i;
		longest_subject_length = max(longest_subject_length,
				length_in_symbols(entry->subject_name));
		longest_grade_sequence = max(longest_grade_sequence,
				entry->length);
	}

	for (uint8_t i = 0; i < table.length; ++i) {
		const GradeTableEntry *entry = table.entries + i;

		const char    *name   = entry->subject_name;
		const uint8_t  length = entry->length;
		const uint8_t *grades = entry->grades;

		printf("%s", name);
		size_t spaces = longest_subject_length - length_in_symbols(name);
		while (spaces--) putc(' ', stdout);

		uint16_t grade_sum = 0;
		for (uint8_t j = 0; j < length; ++j) {
			grade_sum += grades[j];
			printf(" %c", grades[j] + '0');
		}
		spaces = longest_grade_sequence - length;
		while (spaces--) printf("  ");

		float average = length > 0 ? (float)grade_sum / length : 0;
		if (length == 0) {
			printf(" -");
		}
		else {
			printf(" %.2f", average);
		}

		putc('\n', stdout);
	}

	return longest_subject_length + longest_grade_sequence * 2 + 5;
}

#define _move_cursor_up(_n) printf("\033[%uA", _n)
#define _move_cursor_down(_n) printf("\033[%uB", _n)
#define _move_cursor_forward(_n) printf("\033[%uC", _n)
#define _move_cursor_back(_n) printf("\033[%uD", _n)

#define _print_at(_x, _y, _back, ...) { \
	_move_cursor_up(_y); \
	_move_cursor_forward(_x); \
	printf(__VA_ARGS__); \
	_move_cursor_back(_x + _back); \
	_move_cursor_down(_y); \
}

#define _draw_frame_on_i(_c1, _c2) { \
	if (i != 5) _move_cursor_forward(8 * (5 - i)); \
	putc(_c1, stdout); \
	_move_cursor_forward(1); \
	putc(_c2, stdout); \
	_move_cursor_back(3 + 8 * (5 - i)); \
}

#define _clear_j()  _print_at(print_width, height - j + 1, 8, "        ");
#define _update_j() _print_at(print_width, height - j + 1, 8, " -> %.2f", \
		(float)(sums[j] + new_grades_sum) / \
		(entries[j].length + new_grades_amount));

#define _update_grade_count_i() { \
	_move_cursor_forward(4 + 8 * (5 - i)); \
	printf("%*u", 3, new_grades[i]); \
	_move_cursor_back(7 + 8 * (5 - i)); \
}

void start_forecast(const GradeTable table,
		const uint8_t print_width) {
	if (table.length == 0) return;

	const GradeTableEntry *entries = table.entries;

	uint8_t height = table.length;

	uint16_t *sums = calloc(height, sizeof(uint16_t));
	for (uint8_t i = 0; i < table.length; ++i) {
		for (uint8_t j = 0; j < entries[i].length; ++j) {
			sums[i] += entries[i].grades[j];
		}
	}

	_print_at(print_width, height, 8, " -> %.2f",
			(float)sums[0] / entries[0].length);
	printf("\n[5]:  0  4 :  0  3 :  0  2 :  0  1 :  0  0 :  0\n");
	_move_cursor_up(1);
	_move_cursor_back(1);

	uint8_t  new_grades[6] = {};
	uint16_t new_grades_sum = 0;
	uint16_t new_grades_amount = 0;
	uint8_t i = 5, j = 0;

	struct termios old_termios;
	tcgetattr(STDIN_FILENO, &old_termios);

	struct termios new_termios = old_termios;
	new_termios.c_lflag &= ~ICANON & ~ECHO;
	tcsetattr(STDIN_FILENO, TCSANOW, &new_termios);

	char c;
	while ((c = getc(stdin)) != 'q') {
		switch (c) {
			case 'l':
				_draw_frame_on_i(' ', ' ');
				if (--i == 255) i = 5;
				_draw_frame_on_i('[', ']');
				break;
			case 'h':
				_draw_frame_on_i(' ', ' ');
				if (++i == 6) i = 0;
				_draw_frame_on_i('[', ']');
				break;
			case 'J':
				_clear_j();
				if (++j == height) j = 0;
				_update_j();
				break;
			case 'K':
				_clear_j();
				if (--j == 255) j = height - 1;
				_update_j();
				break;
			case 'j':
				if (new_grades[i] != 0) {
					new_grades[i]--;
					_update_grade_count_i();
					new_grades_amount--;
					new_grades_sum -= i;
					_update_j();
				}
				break;
			case 'k':
				if (new_grades[i] != 255) {
					new_grades[i]++;
					_update_grade_count_i();
					new_grades_amount++;
					new_grades_sum += i;
					_update_j();
				}
				break;
		}
	}

	free(sums);
	_move_cursor_down(1);
	tcsetattr(STDIN_FILENO, TCSANOW, &old_termios);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include <curl/curl.h>

#include "grades.h"
#include "format.h"

#define BUFFER_LEN 240

void extract_from_td(const char *str, char *dest) {
	const char *td_open = strstr(str, "<td>");
	if (td_open == NULL) {
		dest[0] = '\0';
		return;
	}

	const char *td_close = strstr(td_open + 4, "</td>");
	bool has_junk = strstr(str, "<tr>") || strstr(str, "<a");
	bool is_comment = strstr(str, "<!--");
	if (!td_close || has_junk || is_comment) {
		dest[0] = '\0';
		return;
	}

	size_t result_len = td_close - td_open - 4;

	for (size_t i = 0; i < result_len; ++i) {
		dest[i] = td_open[i + 4];
	}
	dest[result_len] = '\0';
}

GradeTable get_grade_table(const login_t login,
		const password_t password, bool exclude_empty) {
	char command[sizeof(CURL_COMMAND) + LOGIN_LENGTH + PASSWORD_LENGTH + 1];
    sprintf(command, CURL_COMMAND, login, password);

	GradeTable result = {
		.length = 0,
		.entries = NULL,
	};
	
	FILE *process = popen(command, "r");
	if (process == NULL) return result;
	
	char line[BUFFER_LEN];
	while (!feof(process)) {
		fgets(line, BUFFER_LEN, process);
		if (strstr(line, "<tbody>")) break;
	}

	size_t entries_capacity = 0;
	GradeTableEntry *current_entry = NULL;
	while (!feof(process)) {
		fgets(line, BUFFER_LEN, process);
		if (strstr(line, "</tbody>")) break;

		extract_from_td(line, line);
		if (strcmp(line, "") == 0 || line[1] == '.') continue;

		// need a subject name
		if (!isdigit(line[0])) {
			if (!exclude_empty || current_entry == NULL ||
					current_entry->length != 0) {
				result.length++;
				if (result.length > entries_capacity) {
					entries_capacity = entries_capacity * 2 + 1;
					result.entries = realloc(result.entries,
							entries_capacity * sizeof(GradeTableEntry));
				}
				current_entry = result.entries + result.length - 1;
				current_entry->length = 0;
			}
			strcpy(current_entry->subject_name, line);
		}
		// need a grade
		else {
			current_entry->length++;
			current_entry->grades[current_entry->length - 1] = atoi(line);
		}
	}

	pclose(process);
	return result;
}

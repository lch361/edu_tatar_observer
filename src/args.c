#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#include "args.h"
#include "constants.h"

#define clear_stdin_junk() \
{ int __c; while ((__c = getchar()) != '\n' && __c != EOF) { } }

#define trim_newlines(__buffer) \
{ __buffer[strcspn(__buffer, "\r\n")] = 0; }

bool get_password_from_pass(const login_t login, password_t dest) {
	char command[18 + LOGIN_LENGTH + 1] = "pass edu.tatar.ru/";
	strcat(command + 18, login);

	FILE *process = popen(command, "r");
	if (process == NULL) return false;

	if (fgets(dest, PASSWORD_LENGTH + 1, process) == NULL) return false;
	for (char *i = dest; *i != '\0'; ++i) {
		if (*i == '\n') *i = '\0';
	}
	return (pclose(process) == 0);
}

CliArgs get_cli_args(const int argc, const char **argv) {
	CliArgs result = { .is_request_table    = true,
	                   .exclude_empty_lines = false,
	                   .forecast           = false };

	/*
	 *  --start-date (date only)
	 *  --end-date (date only)
	 */

	bool use_pass = true;
	bool request_set = false;
	for (int i = 1; i < argc; ++i) {
		// in the beginning
		if (strstr(argv[i], "--login=") == argv[i]) {
			strncpy(result.login, argv[i] + 8, LOGIN_LENGTH);
			continue;
		}
		if (strcmp(argv[i], "--no-pass") == 0) {
			use_pass = false;
			continue;
		}
		if (strcmp(argv[i], "--no-empty") == 0) {
			if (result.is_request_table) {
				result.exclude_empty_lines = true;
			}
			continue;
		}
		if (strcmp(argv[i], "--forecast") == 0) {
			if (result.is_request_table) {
				result.forecast = true;
			}
			continue;
		}
		if (!request_set && strstr(argv[i], "--request=") == argv[i]) {
			char request_arg[6];
			strncpy(request_arg, argv[i] + 10, 5);
			if (strcmp(request_arg, "table") == 0) {
				result.is_request_table = true;
				result.exclude_empty_lines = false;
				result.forecast = false;
				request_set = true;
			}
			else if (strcmp(request_arg, "date") == 0) {
				result.is_request_table = false;
				request_set = true;
			}
		}
	}

	if (strcmp(result.login, "") == 0) {
		printf("Enter your login on edu.tatar.ru: ");
		fgets(result.login, LOGIN_LENGTH + 1, stdin);
		trim_newlines(result.login);
		if (result.login[LOGIN_LENGTH - 1] != '\0') {
			clear_stdin_junk();
		}
	}

	bool pass_result = use_pass ? get_password_from_pass(result.login,
			                                             result.password)
	                            : false;
	if (!pass_result) {
		printf("Enter your password: ");

		struct termios old_settings;
		tcgetattr(STDIN_FILENO, &old_settings);

		struct termios new_settings = old_settings;
		new_settings.c_lflag &= ~ECHO;
		tcsetattr(STDIN_FILENO, TCSANOW, &new_settings);

		fgets(result.password, PASSWORD_LENGTH + 1, stdin);
		trim_newlines(result.password);
		putc('\n', stdout);

		tcsetattr(STDIN_FILENO, TCSANOW, &old_settings);
	}

	return result;
}

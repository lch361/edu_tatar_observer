#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "grades.h"
#include "format.h"
#include "args.h"

int main(int argc, const char **argv) {
	CliArgs args = get_cli_args(argc, argv);

	if (args.is_request_table) {
		GradeTable grades_table = get_grade_table(args.login, args.password,
				args.exclude_empty_lines);
		uint8_t print_width = print_grade_table(grades_table);

		if (args.forecast) {
			start_forecast(grades_table, print_width);
		}

		if (grades_table.entries != NULL) {
			free(grades_table.entries);
		}
	}

	return 0;
}

#pragma once

#include "grades.h"

extern uint8_t print_grade_table(const GradeTable table);

extern void start_forecast(const GradeTable table,
		const uint8_t print_width);

#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

#include "constants.h"
#include "typedefs.h"

typedef struct GradeTable GradeTable;
typedef struct GradeTableEntry GradeTableEntry;

typedef char subject_t[200];
typedef uint8_t grades_t[60];

struct GradeTableEntry {
	subject_t subject_name;
	uint8_t length;
	grades_t grades;
};

struct GradeTable {
	uint8_t length;
	GradeTableEntry *entries;
};

extern GradeTable get_grade_table(const login_t login,
		                          const password_t password,
								  bool exclude_empty);

#pragma once

#include "constants.h"

typedef char login_t[LOGIN_LENGTH + 1];
typedef char password_t[PASSWORD_LENGTH + 1];

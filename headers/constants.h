#pragma once

#define LOGIN_LENGTH 10
#define PASSWORD_LENGTH 20

#define COMMAND_LENGTH 1628

#define CURL_COMMAND \
"curl -s 'https://edu.tatar.ru/logon' " \
"-H 'Connection: keep-alive' " \
"-H 'Cache-Control: max-age=0' " \
"-H 'Upgrade-Insecure-Requests: 1' " \
"-H 'Origin: https://edu.tatar.ru' " \
"-H 'Content-Type: application/x-www-form-urlencoded' " \
"-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' " \
"-H 'DNT: 1' " \
"-H 'Accept-Language: en-US,en;q=0.5' " \
"-H 'Sec-Fetch-Site: same-origin' " \
"-H 'Sec-Fetch-Mode: navigate' " \
"-H 'Sec-Fetch-User: ?1' " \
"-H 'Sec-Fetch-Dest: document' " \
"-H 'Referer: https://edu.tatar.ru/logon' " \
"--data-raw 'main_login2=%s&main_password2=%s' " \
"--compressed " \
"--cookie-jar - | " \
"curl -s 'https://edu.tatar.ru/start/logon-process' " \
"-H 'Connection: keep-alive' " \
"-H 'Cache-Control: max-age=0' " \
"-H 'Upgrade-Insecure-Requests: 1' " \
"-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' " \
"-H 'DNT: 1' " \
"-H 'Accept-Language: en-US,en;q=0.5' " \
"-H 'Sec-Fetch-Site: same-origin' " \
"-H 'Sec-Fetch-Mode: navigate' " \
"-H 'Sec-Fetch-User: ?1' " \
"-H 'Sec-Fetch-Dest: document' " \
"-H 'Referer: https://edu.tatar.ru/logon' " \
"--compressed " \
"--cookie - " \
"--cookie-jar - | " \
"curl -sL 'https://edu.tatar.ru/user/diary/year?term=year' " \
"-H 'Connection: keep-alive' " \
"-H 'Cache-Control: max-age=0' " \
"-H 'Upgrade-Insecure-Requests: 1' " \
"-H 'Origin: https://edu.tatar.ru' " \
"-H 'Content-Type: application/x-www-form-urlencoded' " \
"-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' " \
"-H 'DNT: 1' " \
"-H 'Accept-Language: en-US,en;q=0.5' " \
"-H 'Sec-Fetch-Site: same-origin' " \
"-H 'Sec-Fetch-Mode: navigate' " \
"-H 'Sec-Fetch-User: ?1' " \
"-H 'Sec-Fetch-Dest: document' " \
"-H 'Referer: https://edu.tatar.ru/user/diary/year?term=year' " \
"--data-raw 'term=2' " \
"--compressed " \
"--cookie -"

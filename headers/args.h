#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "typedefs.h"

typedef struct CliArgs CliArgs;

struct CliArgs {
	login_t login;
	password_t password;
	bool is_request_table;
	union {
		struct {
			bool exclude_empty_lines;
			bool forecast;
		};
		struct {
			int start_date;
			int end_date;
		};
	};
};

extern bool get_password_from_pass(const login_t login, password_t dest);

extern CliArgs get_cli_args(const int argc, const char **argv);
